## About the project

this is a 15 pizzle game built using:
 - reactJs <br>
 - ES6<br>
 - redux<br>
 - bootstrap<br>
 - jest


## Available Scripts

In the project directory, you can run:



### `npm install`
installs all the relevant modules

### `npm run start`

Runs the app <br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `npm run test`

Launches the test runner in the interactive watch mode.<br>



