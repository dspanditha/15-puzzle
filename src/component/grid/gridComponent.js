import React from 'react';
import { Row, Container, Alert } from 'react-bootstrap';
import BlockContainer from '../block/blockContainer'
import './grid.css'

class gridComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

        this.props.setGrid();
    }


    render() {

        const { blocks, isLoading, won, } = this.props;

        let rows, banner = '';

        if (!isLoading) {
            rows = blocks.map(e => {
                return <BlockContainer key={e.id}
                    xaxis={e.x}
                    yaxis={e.y}
                    value={e.id}
                />

            })

        }



        if (won) {
            banner = <Alert variant="success">
                <Alert.Heading>Yay!!!, you won </Alert.Heading>

            </Alert>;

        }

        return (

            <Container>
                <Row className="grid-4">
                    {rows}
                </Row>
                {banner}
            </Container>
        );
    }
}

export default gridComponent;
