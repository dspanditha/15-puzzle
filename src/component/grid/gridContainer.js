import { connect } from 'react-redux';

import { setGrid } from '../../actions/gridActions'

import gridComponent from './gridComponent';



const mapStateToProps = (state) => {
	return {
		blocks: state.gridStore.blocks,
		isLoading: state.gridStore.gridloading,
		won: state.gridStore.won,

	}
};



const mapDispatchToProps = (dispatch) => {
	return {
		setGrid: () => {
			dispatch(setGrid());
		}
	}

};

const gridContainer = connect(mapStateToProps, mapDispatchToProps)(gridComponent)

export default gridContainer