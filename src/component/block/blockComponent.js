import React from 'react';
import { Assignaxis } from '../../helpers/helpers';
import './block.css'


class blockComponent extends React.Component {



    /**
     * handle block move onclick
     */
    handleClick = (e) => {

        let empty_block_x, empty_block_y, clicked_block_x, clicked_block_y = 0;
        let grid_blocks = this.props.blocks_array;

        const yaxis = e.target.getAttribute('yaxis');
        const xaxis = e.target.getAttribute('xaxis');
        const id = e.target.value

        const empty_block = grid_blocks.filter(block => block.id == 0)
        const clicked_block = grid_blocks.filter(block => block.id == id)
        const clicked_index = grid_blocks.indexOf(clicked_block[0])
        const empty_index = grid_blocks.indexOf(empty_block[0])


        empty_block_x = empty_block[0].x
        empty_block_y = empty_block[0].y

        clicked_block_x = clicked_block[0].x
        clicked_block_y = clicked_block[0].y




        if ((Number(empty_block_y) === (Number(yaxis) - 1) || Number(empty_block_y) === (Number(yaxis) + 1)) && (Number(empty_block_x) === Number(xaxis))) {

            let reassigned_array = Assignaxis('Y', grid_blocks, empty_index, clicked_index, empty_block_y, clicked_block_y)
            this.props.updateGrid(reassigned_array);

        } else if ((Number(empty_block_x) === (Number(xaxis) + 1) || Number(empty_block_x) === (Number(xaxis) - 1)) && (Number(empty_block_y) === Number(yaxis))) {

            let reassigned_array = Assignaxis('X', grid_blocks, empty_index, clicked_index, empty_block_x, clicked_block_x)
            this.props.updateGrid(reassigned_array);

        }

    }

    render() {

        const { value, xaxis, yaxis, won } = this.props;

        let rows = '';

        if (value > 0) {
            rows = <button key={value} xaxis={xaxis} yaxis={yaxis} value={value} onClick={this.handleClick} className="col-md-3 col-sm-3  col-xs-3  btn btn-outline-primary">{value}</button>
        } else {
            rows = <button key={value} xaxis={xaxis} yaxis={yaxis} value={value} onClick={this.handleClick} className="col-md-3 col-sm-3  col-xs-3  btn  btn-default" disabled></button>
        }


        return (
            <React.Fragment>
                {rows}
            </React.Fragment>


        );
    }
}

export default blockComponent;
