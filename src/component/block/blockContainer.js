import { connect } from 'react-redux';

import { updateGrid } from '../../actions/gridActions'

import BlockComponent from './blockComponent';



const mapStateToProps = (state, props) => {

    return {
        blocks_array: state.gridStore.blocks,
        xaxis: props.xaxis,
        yaxis: props.yaxis,
        value: props.value
    }
};



const mapDispatchToProps = (dispatch) => {
    return {
        updateGrid: (payload) => {
            dispatch(updateGrid(payload));
        }
    }

};

const BlockContainer = connect(mapStateToProps, mapDispatchToProps)(BlockComponent)

export default BlockContainer