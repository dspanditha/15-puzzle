import { GRID } from '../helpers/constants'

const initialState = {
    blocks: [],
    gridloading: false,
    error: null,
    won: false
}

function gridReducer(state = initialState, action) {


    switch (action.type) {
        case GRID.BUILD_BEGIN:
            return {
                ...state,
                gridloading: true,
                error: null,
                won: false
            };

        case GRID.BUILD_COMPLETE:
            const blocks_arr = JSON.parse(JSON.stringify(action.payload));

            return {
                ...state,
                blocks: blocks_arr,
                gridloading: false,
                error: null,
                won: false
            };

        case GRID.UPDATE:
            const blocks_update_arr = JSON.parse(JSON.stringify(action.payload));
            return {
                ...state,
                blocks: blocks_update_arr,
                gridloading: false,
                error: null,
                won: false
            };

        case GRID.WON:
            return {
                ...state,
                won: true
            };

        default:
            return state;

    }

};

export default gridReducer;