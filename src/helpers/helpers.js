/**
 * create an random array
 */
export function rearrangeArray(arr, Xaxix, Yaxix) {

    let idx = 0
    for (let i = 0; i < Xaxix; i++) {

        for (let j = 0; j < Yaxix; j++) {
            arr[idx].x = j
            arr[idx].y = i

            idx++
        }

    }

    return arr

}

/**
 * swap empty block with the clicked block
 * @param {*} axis 
 * @param {*} gridArray 
 * @param {*} emptyObjIdx 
 * @param {*} clickedObjIdx 
 * @param {*} emptyAxis 
 * @param {*} clickedAxis 
 */
export function Assignaxis(axis, gridArray, emptyObjIdx, clickedObjIdx, emptyAxis, clickedAxis) {
    let tempEl = null

    tempEl = gridArray[emptyObjIdx]
    gridArray[emptyObjIdx] = gridArray[clickedObjIdx]
    gridArray[clickedObjIdx] = tempEl

    switch (axis) {

        case 'Y': gridArray[clickedObjIdx].y = clickedAxis;
            gridArray[emptyObjIdx].y = emptyAxis;
            break;
        case 'X': gridArray[clickedObjIdx].x = clickedAxis;
            gridArray[emptyObjIdx].x = emptyAxis;
            break;
    }



    return gridArray
}