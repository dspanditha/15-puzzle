import { GRID } from '../helpers/constants'
import Block from '../js/Block'
import { rearrangeArray } from '../helpers/helpers'

/**
 * build the grid
 */
function buildGrid() {

    const Xaxix = 4;
    const Yaxix = 4;
    let blocks = [];
    let id = 0;
    for (let i = 0; i < Xaxix; i++) {

        for (let j = 0; j < Yaxix; j++) {
            blocks.push(new Block(id, j, i))
            id++;
        }

    }


    blocks.sort(function () {
        return .5 - Math.random();
    });

    blocks = rearrangeArray(blocks, Xaxix, Yaxix)

    return Promise.all(blocks);
}



/**
 * check if the array values are ascending
 * @param {*} array 
 */
function isAcending(array) {

    return array.every(function (el, i) {

        return i === 0 || el.id >= array[i - 1];

    })
}



/**
 * setup the grid in reducer
 */
export function setGrid() {
    return dispatch => {
        dispatch(buildGridBegin());
        return buildGrid()
            .then(result => { dispatch(buildGridComplete(result)) })
    };
}


/**
 * update grid on changes
 * @param {*} array 
 */
export function updateGrid(array) {
    return dispatch => {
        dispatch(updateGridComplete(array));
        let win = isAcending(array)
        if (win) {
            dispatch(actionWon())
        }
    };

}


export const buildGridBegin = () => ({
    type: GRID.BUILD_BEGIN
});

export const buildGridComplete = (payload) => ({
    type: GRID.BUILD_COMPLETE,
    payload: payload
});


export const updateGridComplete = (payload) => ({
    type: GRID.UPDATE,
    payload: payload
});

export const actionWon = () => ({
    type: GRID.WON
});