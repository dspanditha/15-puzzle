import React from 'react';
import ReactDOM from 'react-dom';
import App from '../js/App';
import { Provider } from "react-redux";
import store from "../stores/redux-store";
import * as GridActions from '../actions/gridActions'
import Block from '../js/Block'



let blocks = [];
/**
* render app without errors
 */
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}>
    <App />
  </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});


/**
*Test for array 
 */

describe('build grid', () => {
  it('should create an array of random block objects', () => {

    const Xaxix = 4;
    const Yaxix = 4;

    let id = 0;

    for (let i = 0; i < Xaxix; i++) {

      for (let j = 0; j < Yaxix; j++) {

        blocks.push(new Block(id, j, i))
        id++;
      }

    }

    expect(blocks).toHaveLength(id)
  })
})

/**
*Test for grid actions 
 */

describe('ACTION BUILD_BEGIN', () => {
  it('should be fired when the array build begins', () => {

    const expectedAction = {
      type: 'BUILD GRID BEGIN'
    }

    expect(GridActions.buildGridBegin()).toEqual(expectedAction)
  })
})

describe('ACTION WON', () => {
  it('should be fired when the game is won', () => {

    const expectedAction = {
      type: 'GAME WON'
    }

    expect(GridActions.actionWon()).toEqual(expectedAction)
  })
})
