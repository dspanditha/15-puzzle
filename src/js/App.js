import React from 'react';
import '../assets/css/App.css';

import GridContainer from '../component/grid/gridContainer'


function App() {
  return (
    <div className="App">
      <header className="margin-bottom-50">
        15 Puzzle
      </header>

      <GridContainer />

    </div>
  );
}

export default App;
