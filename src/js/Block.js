class Block {

    constructor(id, x, y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    /**
     * get ID
     */
    getID() {
        return this.id
    }

    /**
     * get x axis
     */
    getX() {
        return this.x
    }

    /**
     * get y axis
     */
    getY() {
        return this.y;
    }

}
export default Block;